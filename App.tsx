import React from 'react'
import Route from './src/Route/Route'
import storeRedux from './src/Redux/Store'
import { Provider } from 'react-redux'

const App = () => {
  return (
    <Provider store={storeRedux}>
      <Route/>
    </Provider>
    
  )
}

export default App