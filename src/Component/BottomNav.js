import {View, Text, Image} from 'react-native';
import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import History from '../Activity/App/History';
import Transaksi from '../Activity/App/Transaksi';
import CameraKit from 'react-native-camera-kit';
import Home from '../Activity/App/Home';
import Profile from '../Activity/App/Profile';
import icHome from '../icons/home.png';
import icProfile from '../icons/profile.png';
import icTransaksi2 from '../icons/transaksi2.png';
import icScan from '../icons/scan.png';
import icHistory2 from '../icons/history2.png';

const Tab = createBottomTabNavigator();
const BottomNav = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarShowLabel: false, //untuk menghilangkan label di bottom tab biar bisa styling sendiri text labelnya
      }}>
      {/* tab screen untuk ngasih alamat yang ada di bottom tab biar bisa dipakai */}
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarIcon: ({size, color}) => (
            <View>
              <Image
                source={icHome}
                style={{
                  height: 30,
                  width: 30,
                  resizeMode: 'contain',
                  marginTop: 2,
                }}
              />
              <Text>Home</Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="Transaksi"
        component={Transaksi}
        options={{
          tabBarIcon: ({size, color}) => (
            <View>
              <Image
                source={icTransaksi2}
                style={{
                  height: 30,
                  width: 30,
                  resizeMode: 'contain',
                  justifyContent: 'center',
                  marginTop: 2,
                }}
              />
              <Text>Transaksi</Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="Scan"
        component={Transaksi}
        options={{
          tabBarIcon: ({size, color}) => (
            <View>
              <Image
                source={icScan}
                style={{
                  height: 30,
                  width: 30,
                  resizeMode: 'contain',
                  justifyContent: 'center',
                  marginTop: 2,
                }}
              />
              <Text>Scan</Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="History"
        component={History}
        options={{
          tabBarIcon: ({size, color}) => (
            <View>
              <Image
                source={icHistory2}
                style={{
                  height: 30,
                  width: 30,
                  resizeMode: 'contain',
                  justifyContent: 'center',
                  marginTop: 2,
                }}
              />
              <Text>History</Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarIcon: ({size, color}) => (
            <View>
              <Image
                source={icProfile}
                style={{
                  height: 30,
                  width: 30,
                  resizeMode: 'contain',
                  justifyContent: 'center',
                  marginTop: 2,
                }}
              />
              <Text>Profile</Text>
            </View>
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default BottomNav;
