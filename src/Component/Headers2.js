import {View, Text, StyleSheet, Image, TouchableOpacity, TextInput} from 'react-native';
import React, { useState } from 'react';

const Headerss = ({title, KetikanUser, UbahKetikanUser, navigation, onPress1, onPress2}) => {
  const [text, settext] = useState('');
  return (
    <View style={styles.header}>
      <View style={styles.back}>
        <View>
          <TouchableOpacity onPress={() => navigation.navigate('Home')}>
            <Image
              source={{
                uri: 'https://img.icons8.com/external-royyan-wijaya-detailed-outline-royyan-wijaya/512/external-arrow-arrow-line-royyan-wijaya-detailed-outline-royyan-wijaya-5.png',
              }}
              style={{
                height: 23,
                width: 40,
                resizeMode: 'contain',
              }}
            />
          </TouchableOpacity>
        </View>
        <View>
          <Text style={styles.text}>{title}</Text>
        </View>
      </View>

      <View
        style={{
          flexDirection: 'row',
        }}>
        <View style={styles.cari}>
          <View>
            <Image
              source={{
                uri: 'https://img.icons8.com/ios-filled/512/search--v1.png',
              }}
              style={{height: 23, width: 40, resizeMode: 'contain'}}
            />
          </View>
          <TextInput
            style={{textAlign: 'left', color: '#333333'}}
            onChangeText={text => {
              settext(text);
              UbahKetikanUser(text);
            }}
            value={KetikanUser}
            placeholder="search name or type of transaction"
          />
        </View>
        {/* ASC DSC */}
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-end',
          }}>
          <TouchableOpacity onPress={onPress1}>
            <View
              style={{
                width: '5%',
                alignItems: 'flex-start',
                justifyContent: 'flex-start',
              }}>
              <Image
                source={{
                  uri: 'https://img.icons8.com/pastel-glyph/512/sort-amount-up.png',
                }}
                style={{
                  height: 23,
                  width: 40,
                  resizeMode: 'contain',
                  marginTop: 5,
                  marginRight: -6,
                }}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={onPress2}>
            <View
              style={{
                width: '5%',
                alignItems: 'flex-start',
                justifyContent: 'flex-start',
              }}>
              <Image
                source={{
                  uri: 'https://img.icons8.com/pastel-glyph/512/generic-sorting.png',
                }}
                style={{
                  height: 23,
                  width: 40,
                  resizeMode: 'contain',
                  marginTop: 5,
                }}
              />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    padding: 10,
    backgroundColor: 'white',
    elevation: 5,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    marginBottom: 3,
  },
  back: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginBottom: 5,
  },
  text: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'black',
  },
  cari: {
    backgroundColor: '#f7f7f8',
    borderWidth: 1,
    borderRadius: 16,
    borderColor: '#d9d9d9',
    height: 40,
    width: '81%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginLeft: 5,
  },
});

export default Headerss;
