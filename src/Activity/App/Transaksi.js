import {
  View,
  Text,
  BackHandler,
  StyleSheet,
  KeyboardAvoidingView,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {RadioButton} from 'react-native-paper';
import Headers from '../../Component/Headers';
import {useDispatch} from 'react-redux';
import Modal from 'react-native-paper';
import {action} from '../../Redux/Action/Action';
import icTransaction from '../../icons/transaction.png';

const Transaksi = ({navigation}) => {
  const HandlerBack = () => {
    navigation.goBack();
    return true;
  };
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', HandlerBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', HandlerBack);
    };
  }, []);

  const [disables, setDisables] = useState(false);
  const [dataAmount, setDataAmount] = useState('');
  const [dataSender, setDataSender] = useState('');
  const [dataTarget, setDataTarget] = useState('');
  const [dataType, setDataType] = useState('');
  const [showModal, setShowModal] = useState(false);
  const dispatch = useDispatch();

  const ValidatorButton = () => {
    return dataAmount !== '' &&
      dataSender !== '' &&
      dataTarget !== '' &&
      dataType !== ''
      ? setDisables(false)
      : setDisables(true);
  };
  useEffect(() => {
    ValidatorButton();
  }, [dataAmount, dataSender, dataTarget, dataType]);

  const showToast = () => {
    ToastAndroid.show(
      'Congrats ' +
        dataSender +
        ', You send' +
        ' Rp' +
        dataAmount +
        ' to ' +
        dataTarget,
      ToastAndroid.SHORT,
    );
  };

  const rupiah = x => {
    return x
      ?.toString()
      .replace(/\./g, '')
      .replace(/(\d)(?=(\d{3})+$)/g, '$1' + '.');
  };

  async function pay(e) {
    try {
      dispatch(PayTransaction(dataAmount, dataSender, dataTarget, dataType));
      console.log('Transaksi Berhasil');
    } catch (e) {
      console.log('error', e);
    }
  }

  const handlePay = () => {
    pay();
    navigation.navigate('History');
    showToast();
  };
  const Modal = () => {
    setShowModal(true);
  };
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      style={{backgroundColor: 'white', flex: 1}}>
      <SafeAreaView>
        <ScrollView>
          <View>
            <Headers
              title={'Transaksi'}
              navigation={navigation}
              onPress={() => {
                navigation.goBack();
              }}
            />
            <View>
              <Image
                source={icTransaction}
                style={{
                  height: 170,
                  width: 200,
                  resizeMode: 'contain',
                  alignSelf: 'center',
                  marginTop: 10,
                }}
              />
            </View>
            <View
              style={{
                borderWidth: 1,
                margin: 20,
                // borderColor: '#d9d9d9',
                borderRadius: 20,
                backgroundColor: '#e2e6e9',
                padding: 10,
              }}>
              <Text
                style={{
                  marginTop: 5,
                  marginLeft: 20,
                  color: 'black',
                  fontFamily: 'Questrial-Regular',
                }}>
                Type of Transaction
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-evenly',
                  margin: 3,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <RadioButton
                    value="kontak"
                    status={dataType === 'kontak' ? 'checked' : 'unchecked'}
                    onPress={() => setDataType('kontak')}
                    color="#0F5E38"
                  />
                  <Text
                    style={{
                      fontFamily: 'Questrial-Regular',
                    }}>
                    Send to Contact
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <RadioButton
                    value="bank"
                    status={dataType === 'bank' ? 'checked' : 'unchecked'}
                    onPress={() => setDataType('bank')}
                    color="#0F5E38"
                  />
                  <Text
                    style={{
                      fontFamily: 'Questrial-Regular',
                    }}>
                    Send to Bank
                  </Text>
                </View>
              </View>
              <TextInput
                style={styles.txt}
                placeholder="amount"
                placeholderTextColor={'black'}
                keyboardType={'number-pad'}
                value={dataAmount}
                onChangeText={am => {
                  setDataAmount(rupiah(am || ''));
                }}
              />

              <TextInput
                style={styles.txt}
                placeholder="sender"
                placeholderTextColor={'black'}
                value={dataSender}
                onChangeText={send => {
                  setDataSender(send);
                }}
              />

              <TextInput
                style={styles.txt}
                placeholder="receiver"
                placeholderTextColor={'black'}
                value={dataTarget}
                onChangeText={tg => {
                  setDataTarget(tg);
                }}
              />
            </View>

            <View>
              <TouchableOpacity
                disabled={disables}
                onPress={() => {
                  handlePay();
                }}
                style={{
                  backgroundColor: disables == false ? '#b600a0' : 'black',
                  margin: 20,
                  padding: 10,
                  borderRadius: 8,
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: disables == false ? 'white' : 'black',
                    fontSize: 17,
                    fontFamily: 'Questrial-Regular',
                  }}>
                  Send
                </Text>
              </TouchableOpacity>
              <Modal>
                animationType="fade" transparent={false}
                visible={showModal}
                onRequestClose={() => Modal(false)}
                <View
                  style={{
                    flex: 1,
                    backgroundColor: 'black',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <View
                    style={{
                      backgroundColor: '#b700a1',
                      padding: 95,
                      borderRadius: 10,
                    }}>
                    <Text
                      style={{
                        fontSize: 30,
                        fontWeight: 'bold',
                        marginBottom: 10,
                        textAlign: 'center',
                      }}>
                      Status
                    </Text>
                    <Text style={{fontSize: 16, color: 'white'}}></Text>
                    <Text style={{fontSize: 16}}>Anda Berhasil Masuk</Text>
                    <Text style={{fontSize: 16}}>Selamat Datang !!</Text>
                    <TouchableOpacity onPress={() => handleSubmit()}>
                      <Text
                        style={{
                          fontWeight: 'bold',
                          color: 'white',
                          marginTop: 10,
                          top: 70,
                          textAlign: 'center',
                          fontSize: 25,
                        }}>
                        OK
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </Modal>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  txt: {
    height: 50,
    borderWidth: 1,
    borderRadius: 8,
    marginHorizontal: 15,
    paddingHorizontal: 10,
    backgroundColor: 'white',
    borderColor: '#0F5E38',
    alignItems: 'center',
    marginTop: 5,
    fontFamily: 'Questrial-Regular',
    marginBottom: 5,
  },
});

export default Transaksi;
