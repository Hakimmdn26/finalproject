import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
  ToastAndroid,
  Modal,
} from 'react-native';
import React from 'react';
import icSaldo from '../../icons/saldo.png';
import icData from '../../icons/paydata.png';
import icPulsa from '../../icons/paypulsa.png';
import icTran from '../../icons/paytran.png';
import icVoc from '../../icons/voucher.png';
import icGame from '../../icons/game.png';
import icEmo from '../../icons/emoney.png';
import icHist from '../../icons/history.png';
import icMore from '../../icons/more.png';
import icLogo from '../../icons/logo.png';
import icIklan1 from '../../icons/iklan1.png';
import icIklan2 from '../../icons/iklan2.png';
import icIklan3 from '../../icons/iklan3.png';
import icIklan4 from '../../icons/iklan4.png';
import icIklan5 from '../../icons/iklan5.png';
import icIklan6 from '../../icons/iklan6.png';
import icIklan7 from '../../icons/iklan7.png';
import {useDispatch} from 'react-redux';
import {Logout} from '../../Redux/Action/Action';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Home = ({navigation}) => {
  //REDUX
  const dispatch = useDispatch();

  async function logout() {
    try {
      console.log('logout');
      dispatch(Logout());
      await AsyncStorage.removeItem('Token');
    } catch (e) {
      console.log('error', e);
    }
  }
  const showToast = () => {
    ToastAndroid.show('You are Successfully LoggedOut', ToastAndroid.SHORT);
  };

  return (
    <View style={styles.container}>
      <ScrollView>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            alignContent: 'center',
            marginTop: -5,
          }}>
          <Image
            source={icLogo}
            style={{
              height: 60,
              width: 250,
              resizeMode: 'stretch',
              marginTop: 5,
            }}
          />
          <View>
            <View
              onTouchEnd={async () => {
                logout();
                showToast();
              }}>
              <Image
                source={{
                  uri: 'https://img.icons8.com/fluency-systems-filled/512/exit.png',
                }}
                style={{
                  height: 28,
                  width: 28,
                }}
              />
            </View>
          </View>
        </View>
        {/* SALDO */}
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            borderWidth: 5,
            borderRadius: 10,
            padding: 12,
            borderColor: 'black',
          }}>
          <View
            style={{
              flexDirection: 'column',
            }}>
            <Text
              style={{
                fontFamily: 'Questrial-Regular',
                fontSize: 14,
                color: 'black',
                fontWeight: 'bold',
              }}>
              Saldo Anda
            </Text>
            <Text
              style={{
                fontFamily: 'Questrial-Regular',
                fontSize: 20,
                color: 'black',
                marginTop: 10,
              }}>
              Rp 123.099
            </Text>
          </View>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              alignContent: 'center',
            }}>
            <Image
              source={icSaldo}
              style={{
                height: 40,
                width: 40,
                resizeMode: 'contain',
                marginTop: 5,
              }}
            />
            <Text
              style={{
                fontFamily: 'Questrial-Regular',
                color: '#0F5E38',
              }}>
              TopUp
            </Text>
          </View>
        </View>

        <View
          style={{
            flexDirection: 'row',
          }}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 10,
                marginBottom: 10,
              }}>
              <View
                style={{
                  height: 200,
                  width: Platform.OS == 'android' ? 240 : 220,
                  backgroundColor: 'white',
                  marginLeft: 10,
                  justifyContent: 'center',
                  alignContent: 'center',
                }}>
                <Image
                  source={icIklan1}
                  style={{
                    height: 200,
                    width: Platform.OS == 'android' ? 230 : 210,
                    borderColor: 'black',
                    borderWidth: 3,
                  }}
                />
              </View>

              <View
                style={{
                  height: 200,
                  width: Platform.OS == 'android' ? 240 : 220,
                  marginLeft: 10,
                  backgroundColor: 'white',
                  justifyContent: 'center',
                  alignContent: 'center',
                }}>
                <Image
                  source={icIklan2}
                  style={{
                    height: 200,
                    width: Platform.OS == 'android' ? 230 : 210,
                    borderColor: 'black',
                    borderWidth: 3,
                  }}
                />
              </View>
              <View
                style={{
                  height: 200,
                  width: Platform.OS == 'android' ? 240 : 220,
                  marginLeft: 10,
                  backgroundColor: 'white',
                  justifyContent: 'center',
                  alignContent: 'center',
                }}>
                <Image
                  source={icIklan3}
                  style={{
                    height: 200,
                    width: Platform.OS == 'android' ? 230 : 210,
                    borderColor: 'black',
                    borderWidth: 3,
                  }}
                />
              </View>
              <View
                style={{
                  height: 200,
                  width: Platform.OS == 'android' ? 240 : 220,
                  marginLeft: 10,
                  backgroundColor: 'white',
                  justifyContent: 'center',
                  alignContent: 'center',
                }}>
                <Image
                  source={icIklan3}
                  style={{
                    height: 200,
                    width: Platform.OS == 'android' ? 230 : 210,
                    borderColor: 'black',
                    borderWidth: 3,
                  }}
                />
              </View>
            </View>
          </ScrollView>
        </View>
        {/* PAYMENT */}
        <View
          style={{
            marginTop: 15,
            backgroundColor: 'black',
            borderRadius: 4,
          }}>
          <Text
            style={{
              fontFamily: 'Questrial-Regular',
              color: 'white',
              fontSize: 18,
              marginLeft: 12,
            }}>
            Payment
          </Text>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 10,
              marginLeft: 15,
              marginRight: 15,
              justifyContent: 'space-between',
            }}>
            {/* PAYMENT 1 */}
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                alignContent: 'center',
              }}>
              <View
                style={{
                  width: 50,
                  height: 50,
                  backgroundColor: '#b600a0',
                  borderRadius: 5,
                  borderWidth: 1,
                  borderColor: '#b600a0',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={icData}
                  style={{
                    height: 38,
                    width: 38,
                  }}
                />
              </View>
              <Text
                style={{
                  fontFamily: 'Questrial-Regular',
                  fontSize: 13,
                  color: 'white',
                }}>
                PayData
              </Text>
            </View>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                alignContent: 'center',
              }}>
              <View
                style={{
                  width: 50,
                  height: 50,
                  backgroundColor: '#b600a0',
                  borderRadius: 5,
                  borderWidth: 1,
                  borderColor: '#b600a0',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={icPulsa}
                  style={{
                    height: 38,
                    width: 38,
                  }}
                />
              </View>
              <Text
                style={{
                  fontFamily: 'Questrial-Regular',
                  fontSize: 13,
                  color: 'white',
                }}>
                PayPulsa
              </Text>
            </View>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                alignContent: 'center',
              }}>
              <View
                style={{
                  width: 50,
                  height: 50,
                  backgroundColor: '#b600a0',
                  borderRadius: 5,
                  borderWidth: 1,
                  borderColor: '#b600a0',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={icEmo}
                  style={{
                    height: 38,
                    width: 38,
                  }}
                />
              </View>
              <Text
                style={{
                  fontFamily: 'Questrial-Regular',
                  fontSize: 13,
                  color: 'white',
                }}>
                E-Money
              </Text>
            </View>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                alignContent: 'center',
              }}>
              <View
                style={{
                  width: 50,
                  height: 50,
                  backgroundColor: '#B700A1',
                  borderRadius: 5,
                  borderWidth: 1,
                  borderColor: '#B700A1',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={icGame}
                  style={{
                    height: 38,
                    width: 38,
                    color: 'white',
                  }}
                />
              </View>
              <Text
                style={{
                  fontFamily: 'Questrial-Regular',
                  fontSize: 13,
                  color: 'white',
                }}>
                Games
              </Text>
            </View>
          </View>

          {/* PAYMENY 2 */}
          <View
            style={{
              flexDirection: 'row',
              marginTop: 10,
              marginLeft: 15,
              marginRight: 15,
              justifyContent: 'space-between',
            }}>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                alignContent: 'center',
              }}>
              <View
                style={{
                  width: 50,
                  height: 50,
                  borderRadius: 5,
                  borderWidth: 1,
                  borderColor: '#b600a0',
                  backgroundColor: '#b600a0',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={icVoc}
                  style={{
                    height: 38,
                    width: 38,
                  }}
                />
              </View>
              <Text
                style={{
                  fontFamily: 'Questrial-Regular',
                  fontSize: 13,
                  color: 'white',
                }}>
                Vocher
              </Text>
            </View>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                alignContent: 'center',
              }}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('Transaksi');
                }}>
                <View
                  style={{
                    width: 50,
                    height: 50,
                    backgroundColor: '#b600a0',
                    borderRadius: 5,
                    borderWidth: 1,
                    borderColor: '#b600a0',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Image
                    source={icTran}
                    style={{
                      height: 38,
                      width: 38,
                    }}
                  />
                </View>
              </TouchableOpacity>
              <Text
                style={{
                  fontFamily: 'Questrial-Regular',
                  fontSize: 13,
                  color: 'white',
                }}>
                Transaksi
              </Text>
            </View>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                alignContent: 'center',
              }}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('History');
                }}>
                <View
                  style={{
                    width: 50,
                    height: 50,
                    backgroundColor: '#b600a0',
                    borderRadius: 5,
                    borderWidth: 1,
                    borderColor: '#b600a0',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Image
                    source={icHist}
                    style={{
                      height: 38,
                      width: 38,
                    }}
                  />
                </View>
              </TouchableOpacity>
              <Text
                style={{
                  fontFamily: 'Questrial-Regular',
                  fontSize: 13,
                  color: 'white',
                }}>
                History
              </Text>
            </View>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                alignContent: 'center',
              }}>
              <View
                style={{
                  width: 50,
                  height: 50,
                  borderRadius: 5,
                  borderWidth: 1,
                  borderColor: '#b600a0',
                  backgroundColor: '#b600a0',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={icMore}
                  style={{
                    height: 38,
                    width: 38,
                  }}
                />
              </View>
              <Text
                style={{
                  fontFamily: 'Questrial-Regular',
                  fontSize: 13,
                  color: 'white',
                }}>
                More
              </Text>
            </View>
          </View>
          {/* PROMO&DISCOUNT */}
        </View>
        <View
          style={{
            marginTop: 15,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              alignContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'Questrial-Regular',
                color: 'black',
                fontSize: 18,
              }}>
              Promo and Discount
            </Text>
            <Text
              style={{
                fontFamily: 'Questrial-Regular',
                color: '#0F5E38',
                fontSize: 15,
              }}>
              See More
            </Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
          }}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 10,
                marginBottom: 10,
              }}>
              <View
                style={{
                  height: 200,
                  width: Platform.OS == 'android' ? 240 : 220,
                  backgroundColor: 'white',
                  marginLeft: 10,
                  justifyContent: 'center',
                  alignContent: 'center',
                }}>
                <Image
                  source={icIklan7}
                  style={{
                    height: 200,
                    width: Platform.OS == 'android' ? 230 : 210,
                  }}
                />
              </View>

              <View
                style={{
                  height: 200,
                  width: Platform.OS == 'android' ? 240 : 220,
                  marginLeft: 10,
                  backgroundColor: 'white',
                  justifyContent: 'center',
                  alignContent: 'center',
                }}>
                <Image
                  source={icIklan6}
                  style={{
                    height: 200,
                    width: Platform.OS == 'android' ? 230 : 210,
                  }}
                />
              </View>
              <View
                style={{
                  height: 200,
                  width: Platform.OS == 'android' ? 240 : 220,
                  marginLeft: 10,
                  backgroundColor: 'white',
                  justifyContent: 'center',
                  alignContent: 'center',
                }}>
                <Image
                  source={icIklan4}
                  style={{
                    height: 200,
                    width: Platform.OS == 'android' ? 230 : 210,
                  }}
                />
              </View>
            </View>
            {/*  */}
          </ScrollView>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    margin: 18,
  },
});

export default Home;
