import {
  BackHandler,
  Image,
  KeyboardAvoidingView,
  ScrollView,
  StyleSheet,
  TextInput,
  ToastAndroid,
  TouchableOpacity,
  View,
  Text,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import Headers from '../../Component/Headers';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useDispatch} from 'react-redux';
import {Regist} from '../../Redux/Action/Action';
import icEm from '../../icons/user.png';
import icPass from '../../icons/password.png';

const Register = ({navigation}) => {
  const HandlerBack = () => {
    navigation.goBack();
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', HandlerBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', HandlerBack);
    };
  }, []);

  const [dataText, setDataText] = useState('');
  const [dataEmail, setdataEmail] = useState('');
  const [dataPass, setdataPass] = useState('');
  const [disables, setDisables] = useState(false);
  const [isLoad, setIsload] = useState(false);
  const dispatch = useDispatch();

  const ValidatorButton = () => {
    return dataEmail !== '' && dataPass !== '' && dataText !== ''
      ? setDisables(false)
      : setDisables(true);
  };
  useEffect(() => {
    ValidatorButton();
  }, [dataEmail, dataPass, dataText]);

  const validateEmail = () => {
    if (dataEmail === '') {
      console.error('Error', 'Email tidak boleh kosong');
      return false;
    }
    if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(dataEmail)) {
      console.error('Error', 'Email tidak valid');
      return false;
    }
    return true;
  };

  const validatePassword = () => {
    if (dataPass === '') {
      console.error('Error', 'Password tidak boleh kosong');
      return false;
    }
    if (dataPass.length < 8) {
      console.error('Error', 'Password minimal 8 karakter');
      return false;
    }
    if (!/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/.test(dataPass)) {
      console.error(
        'Password harus terdiri dari huruf besar, huruf kecil, dan angka',
      );
      return false;
    }
    return true;
  };

  const showToast = () => {
    ToastAndroid.show('Registrasi Berhasil', ToastAndroid.SHORT);
  };

  async function registnii(e) {
    try {
      dispatch(Regist(dataText, dataEmail, dataPass));
    } catch (e) {
      console.log('error', e);
    }
  }

  const handleRegist = () => {
    if (validateEmail() && validatePassword()) {
      registnii();
      navigation.navigate('Login');
      showToast();
    }
  };

  console.log(dataText);
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      style={{backgroundColor: 'white', flex: 1}}>
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <View>
            <View
              style={{
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                marginLeft: 15,
              }}>
              <Image
                source={{
                  uri: 'https://cdn-icons-png.flaticon.com/512/8662/8662284.png',
                }}
                style={{
                  height: 170,
                  width: 170,
                  resizeMode: 'contain',
                  marginTop: 50,
                }}
              />
            </View>
            <View>
              <Text
                style={{
                  marginTop: 15,
                  marginBottom: 13,
                  textAlign: 'right',
                  marginRight: 30,
                  fontFamily: 'Questrial-Regular',
                  fontSize: 18,
                }}>
                Create Your Account
              </Text>
            </View>
            <View
              style={{
                margin: 20,
                borderRadius: 8,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                }}>
                <Image
                  source={icEm}
                  style={{
                    height: 28,
                    width: 28,
                    marginLeft: -5,
                    marginRight: -11,
                  }}
                />
                <TextInput
                  style={styles.txt}
                  placeholder="nama"
                  placeholderTextColor={'black'}
                  value={dataText}
                  onChangeText={text => {
                    setDataText(text);
                  }}
                />
              </View>

              <View
                style={{
                  flexDirection: 'row',
                }}>
                <Image
                  source={icEm}
                  style={{
                    height: 28,
                    width: 28,
                    marginLeft: -5,
                    marginRight: -11,
                  }}
                />
                <TextInput
                  style={styles.txt}
                  placeholder="email"
                  placeholderTextColor={'black'}
                  value={dataEmail}
                  onChangeText={email => {
                    setdataEmail(email);
                  }}
                />
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  marginBottom: -15,
                }}>
                <Image
                  source={icPass}
                  style={{
                    height: 28,
                    width: 28,
                    marginLeft: -5,
                    marginRight: -11,
                  }}
                />
                <TextInput
                  style={styles.txt}
                  placeholder="password"
                  placeholderTextColor={'black'}
                  value={dataPass}
                  secureTextEntry
                  onChangeText={pass => {
                    setdataPass(pass);
                  }}
                />
              </View>
            </View>

            <View>
              <TouchableOpacity
                disabled={disables}
                onPress={() => {
                  handleRegist();
                }}
                style={{
                  backgroundColor: disables == false ? '#0F5E38' : '#e6e6e6',
                  borderRadius: 18,
                  margin: 20,
                  height: 45,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: disables == false ? 'white' : 'black',
                    fontSize: 17,
                    fontFamily: 'Questrial-Regular',
                  }}>
                  Register
                </Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                alignContent: 'center',
              }}>
              <Text
                style={{
                  fontSize: 15,
                  fontFamily: 'Questrial-Regular',
                }}>
                Have account?
              </Text>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('Login');
                }}>
                <Text
                  style={{
                    fontFamily: 'Questrial-Regular',
                    color: '#0F5E38',
                  }}>
                  Go to Login
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  txt: {
    borderWidth: 1,
    marginTop: -5,
    padding: 10,
    margin: 20,
    height: 40,
    alignItems: 'center',
    borderRadius: 8,
    backgroundColor: 'white',
    borderColor: 'black',
    fontFamily: 'Questrial-Regular',
    width: 340,
  },
});
export default Register;
