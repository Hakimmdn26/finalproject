import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Button,
  Alert,
  StyleSheet,
  Image,
  KeyboardAvoidingView,
  SafeAreaView,
  ScrollView,
  BackHandler,
  Modal,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {useDispatch} from 'react-redux';
import {Loginn} from '../../Redux/Action/Action';
import icEm from '../../icons/user.png';
import icPass from '../../icons/password.png';
import icLogin from '../../icons/login.png';
import SplashScreen from './SplashScreen';

const Login = ({navigation}) => {
  const HandlerBack = () => {
    navigation.goBack();
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', HandlerBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', HandlerBack);
    };
  }, []);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [disables, setDisables] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const dispatch = useDispatch();

  // const ValidatorButton = () => {
  //   return email !== '' && password !== ''
  //     ? setDisables(false)
  //     : setDisables(true);
  // };

  // useEffect(() => {
  //   ValidatorButton();
  // }, [email, password]);

  const validateEmail = () => {
    if (email === 'hakim@gmail.com') {
      console.log(email);
      return true;
    } else {
      console;
      if (email === '') {
        console.error('Error', 'Email tidak boleh kosong');
        return false;
      }
      if (email !== setEmail) {
        console.error('Error', 'Email tidak valid');
        return false;
      }
    }
    return false;
  };

  const validatePassword = () => {
    if (password === 'Hakim123') {
      return true;
    } else {
      console;
      if (password === '') {
        console.error('Error', 'Password tidak boleh kosong');
        return false;
      }
      if (password !== setPassword) {
        console.error('Error', 'Password tidak valid');
        return false;
      }
    }
    return false;
  };

  async function loginnii(e) {
    try {
      dispatch(Loginn(email, password));
    } catch (e) {
      console.log('error', e);
    }
  }

  const handleSubmit = () => {
    if (validateEmail() && validatePassword()) {
      loginnii();
      console.log('Asyik Login Boss', email, password);
    }
  };

  // const state = useSelector(state => state.Auth);
  // console.log(state, 'ini datanya');
  const handlePress = () => {
    setShowModal(true);
  };
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      style={{flex: 1}}>
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <View>
            <View
              style={{
                alignItems: 'center',
                marginTop: 50,
                marginBottom: 20,
              }}>
              <Image
                source={icLogin}
                style={{
                  width: 350,
                  height: 220,
                }}
              />
            </View>
            <Text
              style={{
                textAlign: 'right',
                marginRight: 20,
                marginBottom: 20,
                fontSize: 18,
                // fontFamily: 'Questrial-Regular',
              }}>
              Login Lah!
            </Text>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Image
                source={icEm}
                style={{
                  height: 28,
                  width: 28,
                  marginLeft: 12,
                  marginRight: -10,
                }}
              />
              <TextInput
                placeholder="Email"
                placeholderTextColor={'black'}
                value={email}
                onChangeText={e => {
                  setEmail(e);
                }}
                style={styles.txtIn}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Image
                source={icPass}
                style={{
                  height: 28,
                  width: 28,
                  marginLeft: 12,
                  marginRight: -10,
                }}
              />
              <TextInput
                placeholder="Password"
                placeholderTextColor={'black'}
                value={password}
                secureTextEntry
                onChangeText={e => {
                  setPassword(e);
                }}
                // onBlur={validatePassword}
                style={styles.txtIn}
              />
            </View>
            <View>
              <TouchableOpacity
                disabled={disables}
                onPress={() => {
                  handlePress();
                }}
                style={{
                  backgroundColor: disables == false ? '#b700a1' : '#e6e6e6',
                  borderRadius: 18,
                  margin: 20,
                  height: 45,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: disables == false ? 'white' : 'black',
                    // fontFamily: 'Questrial-Regular',
                    fontSize: 17,
                  }}>
                  Login
                </Text>
              </TouchableOpacity>
              <Modal
                animationType="fade"
                transparent={false}
                visible={showModal}
                onRequestClose={() => setShowModal(false)}>
                <View
                  style={{
                    flex: 1,
                    backgroundColor: 'black',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <View
                    style={{
                      backgroundColor: '#b700a1',
                      padding: 95,
                      borderRadius: 10,
                    }}>
                    <Text
                      style={{
                        fontSize: 30,
                        fontWeight: 'bold',
                        marginBottom: 10,
                        textAlign: 'center',
                      }}>
                      Status
                    </Text>
                    <Text style={{fontSize: 16, color: 'white'}}></Text>
                    <Text style={{fontSize: 16}}>Anda Berhasil Masuk</Text>
                    <Text style={{fontSize: 16}}>Selamat Datang !!</Text>
                    <TouchableOpacity onPress={() => handleSubmit()}>
                      <Text
                        style={{
                          fontWeight: 'bold',
                          color: 'white',
                          marginTop: 10,
                          top: 70,
                          textAlign: 'center',
                          fontSize: 25,
                        }}>
                        OK
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </Modal>
            </View>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                alignContent: 'center',
              }}>
              <Text
                style={{
                  fontSize: 15,
                  // fontFamily: 'Questrial-Regular',
                }}>
                Don't have account?
              </Text>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('Register');
                }}>
                <Text
                  style={{
                    // fontFamily: 'Questrial-Regular',
                    color: '#0F5E38',
                  }}>
                  Register
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  txtIn: {
    borderWidth: 1,
    // borderColor: 'red',
    marginTop: -5,
    padding: 10,
    margin: 20,
    height: 40,
    alignItems: 'center',
    borderRadius: 8,
    backgroundColor: 'white',
    borderColor: '#ff66c4',
    // fontFamily: 'Questrial-Regular',
    width: Platform.OS == 'ios' ? 300 : 340,
  },
});

export default Login;
