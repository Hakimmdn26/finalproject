import {View, Text, Image} from 'react-native';
import React, {useEffect} from 'react';
import icSplash from '../../icons/splash.png';

const SplashScreen = ({navigation}) => {
  useEffect(() => {
    const timeout = setTimeout(() => {
      navigation.replace('Login');
    }, 1500);
    return () => clearTimeout(timeout);
  }, []);

  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        backgroundColor: 'white',
      }}>
      <Image
        source={icSplash}
        style={{
          width: 350,
          height: 220,
        }}
      />
      {/* <Text
        style={{
          fontFamily: 'CarterOne-Regular',
          fontSize: 35,
          color: '#0f5e38',
        }}>
        Paya
      </Text>
      <Text
        style={{
          fontFamily: 'BalooBhai2-VariableFont_wght',
          fontSize: 17,
        }}>
        The Best payment
      </Text> */}
    </View>
  );
};

export default SplashScreen;
