import React, {useEffect, useState} from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {NavigationContainer} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import SplashScreen from '../Activity/Auth/SplashScreen';
import Login from '../Activity/Auth/Login';
import Register from '../Activity/Auth/Register';
import Home from '../Activity/App/Home';
import Transaksi from '../Activity/App/Transaksi';
import History from '../Activity/App/History';
import Profile from '../Activity/App/Profile';
import BottomNav from '../Component/BottomNav';
const Stack = createNativeStackNavigator();

const AppStack = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="ButtomNav" component={BottomNav} />
      <Stack.Screen name="Profile" component={Profile} />
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="Transaksi" component={Transaksi} />
      <Stack.Screen name="History" component={History} />
    </Stack.Navigator>
  );
};

const AuthStack = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="SplashScreen" component={SplashScreen} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="Profile" component={Profile} />
    </Stack.Navigator>
  );
};

function Route() {
  const [tokens, settokens] = useState(null);
  // settoken pake async, saaat pencet login otomatis akan buat token
  const UpdateData = useSelector(state => state.Auth.AuthData);
  const DataUser = async () => {
    const token = await AsyncStorage.getItem('Token');
    settokens(token);
  };
  useEffect(() => {
    DataUser();
  }, [UpdateData]);

  return (
    <NavigationContainer>
      {/* <Splash/> */}
      {tokens !== null ? <AppStack /> : <AuthStack />}
      {/* // appstack setelah login, auth sebelum login. */}
      {/* saat logout, akan remove item, jadi token login sebelumnya akan hilang, dan user tidak dapat login */}
    </NavigationContainer>
  );
}

export default Route;
